const Tarea = require("./tarea");
class Tareas {

    _listado = {};

    get listadoArr() {
        const listado = [];
        Object.keys(this._listado).forEach(key => {
            const tarea = this._listado[key];
            listado.push(tarea);
        });
        return listado;
    }

    constructor() {
        this._listado = {};
    }
    borrarTarea(id = '') {
        if (this._listado[id]) {
            delete this._listado[id];
        }
    }
    cargarTareasFromArray(tareas = []) {
        tareas.forEach(tarea => {
            this._listado[tarea.id] = tarea;
        });
    }

    crearTarea(desc = '') {
        const tarea = new Tarea(desc);
        this._listado[tarea.id] = tarea;
    }
    listadoCompleto() {
        this.listadoArr.forEach((tarea, index) => {
            console.log(`${((index + 1) + '.').green} ${tarea.desc} :: ${(tarea.completadoEN) ? 'Completado'.green : 'Pendiente'.red}`);
        });
    }
    listarPendientesCompletadas(completadas = true) {
        let i = 1;
        this.listadoArr.forEach(tarea => {
            if (completadas && tarea.completadoEN !== null) {
                console.log(`${((i++) + '.').green} ${tarea.desc} :: ${(tarea.completadoEN).green}`);
            }
            else if (completadas == false && tarea.completadoEN == null) {
                console.log(`${((i++) + '.').green} ${tarea.desc} :: ${'Pendiente'.red}`);
            }
        });
    }

    toggle(ids = []) {
        ids.forEach(id => {
            const tarea = this._listado[id];
            if (!tarea.completadoEN) {
                tarea.completadoEN = new Date().toISOString();
            }
        });

        this.listadoArr.forEach(tarea => {

            if (!ids.includes(tarea.id)) {
                this._listado[tarea.id].completadoEN = null;
            }

        });
    }
}

module.exports = Tareas